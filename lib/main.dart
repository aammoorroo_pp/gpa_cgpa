import 'package:flutter/material.dart';

import './gpa.dart';
import './cgpa.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: 'Flutter Demo',
        theme: ThemeData(
          primarySwatch: Colors.blue,
          visualDensity: VisualDensity.adaptivePlatformDensity,
        ),
        home: DefaultTabController(
            child: Scaffold(
                appBar: AppBar(
              title: Text("GPA AND CGPA CALCULATOR"),
              backgroundColor: Colors.black45,
              bottom: TabBar(
                tabs: [
                  Tab(
                    text: "GPA",
                  ),
                  Tab(
                    text: "CGPA",
                  )
                ],
              ),
            ),
           
            body: TabBarView(children: [
              Gpa(),
              CGPA(),
            ]),
            ),
             length: 2,
            initialIndex: 0,),);
  }
}