import 'package:flutter/material.dart';
import './gpacalculation.dart';
class Gpa extends StatefulWidget {
  @override
  _GpaState createState() => _GpaState();
}

class _GpaState extends State {
  var selectedType;
  var index;
  
  var subjects = {
    "First Semester": ["Mathematics", "C++", "Chemistry", "Physics"],
    "Second Semester": [
      "Distributed Systems",
      "Java",
      "Data Structures",
      "Digital Principles",
      "System Design"
    ],
    "Third Semester": [
      "Mobile Computing",
      "Graphic Design",
      "Software Systems",
      "TQM",
      "POC"
    ],
    "Fourth Semester": [
      "Compiler Design",
      "Probablity and Queueing Theory",
      "Computer Networks",
      "Operating Systems",
      "DAA"
    ],
    "Fifth Semester": [
      "DSP",
      "Discrete Mathematics",
      "Internet Programming",
      "OOAD",
      "Theory of Computation"
    ],
  };
  var subjectGrades ={
    "Mathematics":4, "C++":4, "Chemistry":3, "Physics":3,
    "Distributed Systems":4,
      "Java":5,
      "Data Structures":3,
      "Digital Principles":4,
      "System Design":2,
       "Mobile Computing":3,
      "Graphic Design":5,
      "Software Systems":3,
      "TQM":3,
      "POC":5,
      "Compiler Design":4,
      "Probablity and Queueing Theory":5,
      "Computer Networks":3,
      "Operating Systems":3,
      "DAA":5,
       "DSP":5,
      "Discrete Mathematics":2,
      "Internet Programming":2,
      "OOAD":3,
      "Theory of Computation":5
};
var gradePoints = {
  "S":10,
  "A":9,
  "B":8,
  "C":7,
  "RA":0,

};
List <int> resultSubs = [];
List <int> resultGrades = [];
 List<String> grade = [];
List<String> selectedSubjects = [];
var gpaFinal;
int isValid(){ int f=0;
       for(int i=0;i<resultGrades.length;i++)
       {
         if(resultGrades[i]==0)
            f=1;
            
       }
       if(resultGrades.length==0)
         return 0;
       if((f==0) && (resultGrades.length ==  selectedSubjects.length))
         return 1;
        else 
           return 2;

}
void resetAll(){
   grade = [];
                        resultSubs=[];
                        resultGrades=[];
                        for (int i = 0; i < selectedSubjects.length; i++) {
                          grade.add("RA");
                          
                        }
}
  @override
  Widget build(BuildContext context) {
    return Scaffold( backgroundColor: Colors.blueGrey,
      body: Column(
        children: <Widget>[
          Container(
            width: 400,
            child: Form(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                //mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  SizedBox(width: 50.0),
                  DropdownButton(
                    items: <String>[
                      "First Semester",
                      "Second Semester",
                      "Third Semester",
                      "Fourth Semester",
                      "Fifth Semester"
                    ].map<DropdownMenuItem<String>>((String value) {
                      return DropdownMenuItem<String>(
                        value: value,
                        child: Text(value),
                      );
                    }).toList(),
                    onChanged: (selectedSemester) {
                      print('${subjects[selectedSemester]}');
                      setState(() {
                        selectedType = selectedSemester;
                        selectedSubjects = subjects[selectedSemester];
                        grade = [];
                        resultSubs=[];
                        resultGrades=[];
                        for (int i = 0; i < selectedSubjects.length; i++) {
                          grade.add("RA");
                          
                        }
                          print(resultSubs);
                      });
                    },
                    value: selectedType,
                    isExpanded: false,
                    hint: Text(
                      'Select Semester',
                      style: TextStyle(color: Colors.cyan),
                    ),
                  )
                ],
              ),
            ),
            color: Colors.blueGrey,
          ),
          Padding(
            padding: EdgeInsets.all(20),
            child: ListView.builder(
                scrollDirection: Axis.vertical,
                shrinkWrap: true,
                itemCount: selectedSubjects.length,
                itemBuilder: (context, index) {
                  
                  resultSubs.add(subjectGrades[selectedSubjects[index]] );
                  
                  return Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Text(selectedSubjects[index]),
                      DropdownButton(
                        items: <String>["S", "A", "B", "C", "RA"]
                            .map<DropdownMenuItem<String>>((String gradeValue) {
                          return DropdownMenuItem<String>(
                            value: gradeValue,
                            child: Text(gradeValue),
                            
                          );
                        }).toList(),
                        onChanged: (selectedGrade) {
                          setState(() {
                            grade[index] = selectedGrade;
                          });
                          resultGrades.add(gradePoints[grade[index]]);
                          
                        },
                        value: grade[index].toString(),
                        isExpanded: false,
                        hint: Text(
                          'Select Grade',
                          style: TextStyle(color: Color(0xff11b719)),
                        ),
                      ),
                      
                      
                    ],
                  );
                }),
          ),
          Container(color: Colors.blueGrey,
            child: Row(mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[ RaisedButton(
            shape: Border.all(color: Colors.lightGreen,style: BorderStyle.solid,width: 1),
            onPressed: (){
            print(grade);
            print(resultSubs);
            print(resultGrades);
            if(isValid()== 1)
           { 
             gpaFinal = gpaCalculation(resultSubs,resultGrades);
             return showDialog(
      context: context,
      builder: (context) {
        return AlertDialog(
          // Retrieve the text the user has entered by using the
          // TextEditingController.
          content: Text("Your GPA is $gpaFinal",style:TextStyle(fontSize: 18,color: Colors.lightGreen),textAlign: TextAlign.center,),
          backgroundColor: Colors.black12,
        );
      },
    );
    }
  else if(isValid()== 2){
          resetAll();
         return showDialog(
      context: context,
      builder: (context) {
        return AlertDialog(
          // Retrieve the text the user has entered by using the
          // TextEditingController.
          content: Text("Enter valid Grade",style:TextStyle(fontSize: 18),textAlign: TextAlign.center,),
          backgroundColor: Colors.greenAccent,
        );
      },
    );
     }
          
    else
    {
      return showDialog(
      context: context,
      builder: (context) {
        return AlertDialog(
          // Retrieve the text the user has entered by using the
          // TextEditingController.
          content: Text("Select Semester",style:TextStyle(fontSize: 18),textAlign: TextAlign.center,),
          backgroundColor: Colors.red,
        );
      },
    );
    }
               },   child: Text("Submit")) ,RaisedButton( child: Text("Reset") ,onPressed: resetAll, shape: Border.all(color: Colors.black38,style: BorderStyle.solid,width: 1),)],)
           )
        ],
      ),
    );
  }
}
