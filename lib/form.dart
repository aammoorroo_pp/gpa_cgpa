



import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';

class CgpaForm extends StatefulWidget {
  @override
  _CgpaFormState createState() => _CgpaFormState();
}

class _CgpaFormState extends State{
   final GlobalKey<FormState> _formKeyValue = new GlobalKey<FormState>();
    bool _validKey = false;
     double cgpa=0;
     double val = 0;
    final myController1 = TextEditingController();
    final myController2 = TextEditingController();
    final myController3 = TextEditingController();
    final myController4 = TextEditingController();
    final myController5 = TextEditingController();

    void clearTextInput()
    {
        myController1.clear();
        myController2.clear();
        myController3.clear();
        myController4.clear();
        myController5.clear();
    }
    bool isFormValid()
    { if ( (myController1.text.trim().length==0)  ||(myController2.text.trim().length==0)  ||(myController3.text.trim().length==0)  ||(myController4.text.trim().length==0)  ||(myController5.text.trim().length==0)  || double.parse(myController1.text)>10.0 || double.parse(myController2.text)>10.0 || double.parse(myController3.text)>10.0 || double.parse(myController4.text)>10.0 || double.parse(myController5.text)>10.0)
      { return false;}
      
        return true;
    }
    void valid(){
      
       setState(() {
      _validKey = true;
    });
    }
 
   
  @override
  Widget build(BuildContext context) {
    return Form( 
        key: _formKeyValue,
        autovalidate: _validKey,
        child: new ListView(
          padding: const EdgeInsets.symmetric(horizontal: 15.0),
          children: <Widget>[
            SizedBox(height: 20.0),
            new TextFormField(
                 
                decoration: const InputDecoration(
                  
                  hintText: 'Enter First Semester GPA ',
                  labelText: 'First Semester',
                ),
                keyboardType: TextInputType.number,
                 controller: myController1,
                
                validator: (value) {  
              if ((value.isEmpty) || double.parse(value)>10.0 ) {  
                return 'Please enter valid gpa';  
              }  
              else
              return null;  
            },  ),
            new TextFormField(
              decoration: const InputDecoration(
                hintText: 'Enter Second Semester GPA',
                labelText: 'Second Semester',
              ),
                keyboardType: TextInputType.number,
                 controller: myController2,
                validator: (value) {  
              if (value.isEmpty || double.parse(value)>10.0) {  
                return 'Please enter valid gpa';  
              }  
              else
              return null;  
            },  
            ),
            new TextFormField(
              decoration: const InputDecoration(
                hintText: 'Enter Third Semester GPA',
                labelText: 'Third Semester',
              ),
              keyboardType: TextInputType.number,
               controller: myController3,
              validator: (value) {  
              if (value.isEmpty || double.parse(value)>10.0) {  
                return 'Please enter valid gpa';  
              } 
              else 
              return null;  
            },  
            ),
             new TextFormField(
              decoration: const InputDecoration(
                hintText: 'Enter Fourth Semester GPA',
                labelText: 'Fourth Semester',
              ),
                keyboardType: TextInputType.number,
                 controller: myController4,
                validator: (value) {  
              if (value.isEmpty || double.parse(value)>10.0) {  
                return 'Please enter valid gpa';  
              } 
              else 
              return null;  
            },  
            ),
             new TextFormField(
              decoration: const InputDecoration(
                hintText: 'Enter Fifth Semester GPA',
                labelText: 'Fifth Semester',
              ),
                keyboardType: TextInputType.number,
                 controller: myController5,
                validator: (value) {  
              if (value.isEmpty || double.parse(value)>10.0) {  
                return 'Please enter valid gpa';  
              }  
              else
              return null;  
            },  
            ),
             new Container(  
              padding: const EdgeInsets.all(15.0),  
              child:Row(children: <Widget>[new RaisedButton(  
                child: const Text('Submit'),  
                  onPressed: (){ 
                     if(isFormValid()==true){
                    cgpa = (double.parse(myController1.text)+double.parse(myController2.text)+double.parse(myController3.text)+double.parse(myController4.text)+double.parse(myController5.text))/5;
                    return showDialog(
      context: context,
      builder: (context) {
        return AlertDialog(
          // Retrieve the text the user has entered by using the
          // TextEditingController.
          content: Text("The CGPA is $cgpa",style:TextStyle(fontSize: 18),textAlign: TextAlign.center,),
          backgroundColor: Colors.greenAccent,
        );
      },
    );
  }
   else{
     valid();
   }
         }
),
RaisedButton( child: const Text('Reset'),onPressed: clearTextInput,)],
                     mainAxisAlignment: MainAxisAlignment.spaceBetween  ) 
                    
              ),  
          ],
        ),
      
      
    );
  }
}